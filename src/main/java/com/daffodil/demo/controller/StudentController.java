package com.daffodil.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.demo.entity.Student;
import com.daffodil.demo.service.IStudentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;
import reactor.core.publisher.Mono;

/**
 * -学生信息控制层
 * @author yweijian
 * @date 2023-03-27
 * @version 1.0
 * @description
 */
@Api(value = "学生信息管理", tags = "学生信息管理")
@RestController
@RequestMapping("/api-demo/school/student")
public class StudentController extends ReactiveBaseController {

    @Autowired
    private IStudentService studentService;

    @ApiOperation("分页查询学生信息列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("school:student:list")
    @GetMapping("/list")
    public Mono<TableResult> list(Student student, Page page, @ApiIgnore ServerHttpRequest request) {
        initQuery(student, page, request);
        List<Student> list = studentService.selectEntityList(query);
        return Mono.just(TableResult.success(list, query));
    }

    @ApiOperation("获取学生信息详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("school:student:info")
    @GetMapping("/info")
    public Mono<JsonResult> info(@ApiParam(value = "学生信息ID") String id, @ApiIgnore ServerHttpRequest request) {
        Student student = studentService.selectEntityById(id);
        return Mono.just(JsonResult.success(student));
    }

    @ApiOperation("新增学生信息")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("school:student:add")
    @OperLog(title = "学生信息管理", type = Business.INSERT)
    @PostMapping("/add")
    public Mono<JsonResult> add(@Validated @RequestBody Student student, @ApiIgnore ServerHttpRequest request) {
        studentService.insertEntity(student);
        return Mono.just(JsonResult.success(student));
    }

    @ApiOperation("修改学生信息")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("school:student:edit")
    @OperLog(title = "学生信息管理", type = Business.UPDATE)
    @PostMapping("/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody Student student, @ApiIgnore ServerHttpRequest request) {
        studentService.updateEntity(student);
        return Mono.just(JsonResult.success(student));
    }
    
    @ApiOperation("删除学生信息")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("school:student:remove")
    @OperLog(title = "学生信息管理", type = Business.DELETE)
    @PostMapping("/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        studentService.deleteEntityByIds(ids);
        return Mono.just(JsonResult.success());
    }

}
