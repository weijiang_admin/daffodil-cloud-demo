package com.daffodil.demo.constant;

/**
 * 
 * @author yweijian
 * @date 2021年12月21日
 * @version 1.0
 * @description
 */
public class DemoConstant {
    
    /**
     * api上下文地址
     */
    public static final String API_CONTENT_PATH = "/api-demo";
    
}
