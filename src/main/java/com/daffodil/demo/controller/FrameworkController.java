package com.daffodil.demo.controller;

import java.util.List;
import java.util.Map;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.Page;
import com.daffodil.demo.constant.DemoConstant;
import com.daffodil.framework.model.Area;
import com.daffodil.framework.model.Dictionary;
import com.daffodil.framework.model.Role;
import com.daffodil.framework.model.Tag;
import com.daffodil.framework.model.User;
import com.daffodil.framework.service.IAreaService;
import com.daffodil.framework.service.IDictionaryService;
import com.daffodil.framework.service.IHolidaysService;
import com.daffodil.framework.service.IRoleService;
import com.daffodil.framework.service.ITagService;
import com.daffodil.framework.service.IUserService;

import io.swagger.annotations.Api;

@Api(value = "基础框架服务", tags = "基础框架服务")
@RestController
@RequestMapping(DemoConstant.API_CONTENT_PATH)
public class FrameworkController {

    @DubboReference
    private IUserService userService;

    @DubboReference
    private IRoleService roleService;

    @DubboReference
    private IDictionaryService dictionaryService;

    @DubboReference
    private IAreaService areaService;

    @DubboReference
    private ITagService tagService;

    @DubboReference
    private IHolidaysService holidaysService;

    @GetMapping("/selectUserById")
    public User selectUserById(@RequestParam(value = "userId") String userId) {
        return userService.selectUserById(userId);
    }

    @GetMapping("/selectUserList")
    public List<User> selectUserList(@RequestParam(value = "userId", required = false) String userId,
        @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum, 
        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        
        User user = new User();
        user.setId(userId);

        Page page = new Page();
        page.setPageNum(pageNum);
        page.setPageSize(pageSize);

        return userService.selectUserList(user, page);
    }

    @GetMapping("/selectRoleList")
    public List<Role> selectRoleList(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum, 
        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        Role role = new Role();

        Page page = new Page();
        page.setPageNum(pageNum);
        page.setPageSize(pageSize);
        return roleService.selectRoleList(role, page);
    }

    @GetMapping("/selectDictionaryByLabel")
    public List<Dictionary> selectDictionaryByLabel(@RequestParam(value = "dictLabel") String dictLabel){
        return dictionaryService.selectDictionaryByLabel(dictLabel);
    }

    @GetMapping("/selectAreaListByParentId")
    public List<Area> selectAreaListByParentId(@RequestParam(value = "parentId") String parentId){
        return areaService.selectAreaListByParentId(parentId);
    }

    @GetMapping("/selectTagByLabel")
    public List<Tag> selectTagByLabel(@RequestParam(value = "tagLabel") String tagLabel){
        return tagService.selectTagByLabel(tagLabel);
    }

    @GetMapping("/selectHolidaysByDate")
    public byte[] selectDaysByYear(@RequestParam(value = "year") Integer year){
        return holidaysService.selectDaysByYear(year);
    }

    @GetMapping("/multiselectDaysByYear")
    public Map<Integer, byte[]> multiselectDaysByYear(@RequestParam(value = "startYear") Integer startYear, @RequestParam(value = "endYear") Integer endYear){
        return holidaysService.multiselectDaysByYear(startYear, endYear);
    }
}
