package com.daffodil.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 费用报销
 * @author yweijian
 * @date 2021年12月21日
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "demo_expense")
public class Expense extends BaseEntity<String> {

    private static final long serialVersionUID = -8918974734251706688L;

    /** 费用报销ID */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "expense_id")
    private String id;
    
    /** 费用报销主题 */
    @Column(name = "title")
    @Hql(type = Logical.LIKE)
    private String title;

    /** 费用报销金额 */
    @Column(name = "money")
    private Double money;

    /** 费用报销清单 */
    @Column(name = "content")
    @Hql(type = Logical.LIKE)
    private String content;
    
    /** 流程模型标识 */
    @Column(name="model_key")
    @Hql(type = Logical.EQ)
    private String modelKey;
    
    /** 创建者 */
    @Column(name="create_by")
    @Hql(type = Logical.EQ)
    private String createBy;

    /** 创建时间 */
    @Column(name="create_time")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by")
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @Column(name="update_time")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark")
    @Hql(type = Logical.LIKE)
    private String remark;
    
}
