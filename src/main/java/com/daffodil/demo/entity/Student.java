package com.daffodil.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.*;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.framework.annotation.Dict;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -学生信息对象
 * @author yweijian
 * @date 2023-03-27
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "demo_student")
public class Student extends BaseEntity<String> {

    private static final long serialVersionUID = 1L;

    /** 主键编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id")
    private String id;

    /** 名称 */
    @Column(name = "name")
    @NotBlank(message = "名称不能为空")
    @Hql(type = Logical.LIKE)
    private String name;

    /** 备注 */
    @Column(name = "remark")
    @Hql(type = Logical.LIKE)
    private String remark;

    /** 创建者 */
    @Column(name = "create_by")
    private String createBy;

    /** 创建时间 */
    @Column(name = "create_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 状态 */
    @Column(name = "status")
    @Hql(type = Logical.EQ)
    @Dict(value = "sys_data_status")
    private String status;

    /** 联系电话 */
    @Column(name = "phone")
    private String phone;

    /** 排序 */
    @Column(name = "sort")
    private Integer sort;

    /** 父级编号 */
    @Column(name = "parent_id")
    private String parentId;

    /** 祖级序列 */
    @Column(name = "ancestors")
    private String ancestors;

}
