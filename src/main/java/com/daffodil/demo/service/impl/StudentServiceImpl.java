package com.daffodil.demo.service.impl;

import org.springframework.stereotype.Service;

import com.daffodil.core.service.impl.BaseEntityServiceImpl;
import com.daffodil.demo.entity.Student;
import com.daffodil.demo.service.IStudentService;

/**
 * -学生信息Service接口业务实现层
 * @author yweijian
 * @date 2023-03-27
 * @version 1.0
 * @description
 */
@Service
public class StudentServiceImpl extends BaseEntityServiceImpl<String, Student> implements IStudentService {

}
