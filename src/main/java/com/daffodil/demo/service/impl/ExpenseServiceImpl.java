package com.daffodil.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.core.util.HqlUtils;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.demo.entity.Expense;
import com.daffodil.demo.service.IExpenseService;
import com.daffodil.util.StringUtils;

/**
 * 
 * @author yweijian
 * @date 2021年12月21日
 * @version 1.0
 * @description
 */
@Service
public class ExpenseServiceImpl implements IExpenseService{

    @Autowired
    private JpaDao<String> jpaDao;
    
    @Override
    public List<Expense> selectExpenseList(Query<Expense> query) {
        StringBuffer hql = new StringBuffer("from Expense where 1=1 ");
        List<Object> paras = new ArrayList<Object>();
        HqlUtils.createHql(hql, paras, query);
        return jpaDao.search(hql.toString(), paras, Expense.class, query.getPage());
    }

    @Override
    public Expense selectExpenseById(String expenseId) {
        return jpaDao.find(Expense.class, expenseId);
    }

    @Override
    @Transactional
    public void insertExpense(Expense expense) {
        jpaDao.save(expense);
    }

    @Override
    @Transactional
    public void updateExpense(Expense expense) {
        jpaDao.update(expense);
    }

    @Override
    @Transactional
    public void deleteExpenseByIds(String[] ids) {
        if(StringUtils.isNotEmpty(ids)){
            jpaDao.delete(Expense.class, ids);
        }
    }

}
