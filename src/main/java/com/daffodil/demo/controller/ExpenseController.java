package com.daffodil.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.demo.constant.DemoConstant;
import com.daffodil.demo.entity.Expense;
import com.daffodil.demo.service.IExpenseService;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.framework.model.LoginUser;
import com.daffodil.util.text.Convert;

/**
 * 费用报销 信息控制层
 * @author yweijian
 * @date 2021年12月21日
 * @version 1.0
 * @description
 */
@RestController
@RequestMapping(DemoConstant.API_CONTENT_PATH)
public class ExpenseController extends ReactiveBaseController {

    @Autowired
    private IExpenseService expenseService;

    /**
     * 查询费用报销列表
     */
    @SuppressWarnings("unchecked")
    @AuthPermission("demo:expense:list")
    @GetMapping("/expense/list")
    public TableResult list(Expense expense, Page page, LoginUser loginUser, ServerHttpRequest request) {
        expense.setCreateBy(loginUser.getLoginName());
        initQuery(expense, page, request);
        List<Expense> list = expenseService.selectExpenseList(query);
        return TableResult.success(list, query);
    }
    
    @GetMapping("/expense/info")
    public JsonResult info(String id, ServerHttpRequest request) {
        Expense expense = expenseService.selectExpenseById(id);
        return JsonResult.success(expense);
    }

    /**
     * 新增保存费用报销
     */
    @AuthPermission("demo:expense:add")
    @OperLog(title = "费用报销", type = Business.INSERT)
    @PostMapping("/expense/add")
    public JsonResult add(@Validated @RequestBody Expense expense, ServerHttpRequest request) {
        expenseService.insertExpense(expense);
        return JsonResult.success(expense);
    }

    /**
     * 修改保存费用报销
     */
    @AuthPermission("demo:expense:edit")
    @OperLog(title = "费用报销", type = Business.UPDATE)
    @PostMapping("/expense/edit")
    public JsonResult edit(@Validated @RequestBody Expense expense, ServerHttpRequest request) {
        expenseService.updateExpense(expense);
        return JsonResult.success(expense);
    }

    /**
     * 删除费用报销
     */
    @AuthPermission("demo:expense:remove")
    @OperLog(title = "费用报销", type = Business.DELETE)
    @PostMapping("/expense/remove")
    public JsonResult remove(String ids, ServerHttpRequest request) {
        expenseService.deleteExpenseByIds(Convert.toStrArray(ids));
        return JsonResult.success();
    }
}
