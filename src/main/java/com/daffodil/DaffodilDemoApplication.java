package com.daffodil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 
 * @author yweijian
 * @date 2021年12月21日
 * @version 1.0
 * @description
 */
@EnableDiscoveryClient
@SpringBootApplication
public class DaffodilDemoApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(DaffodilDemoApplication.class);
        application.setWebApplicationType(WebApplicationType.REACTIVE);
        application.addListeners(new ApplicationPidFileWriter());
        application.run(args);
    }
    
}
