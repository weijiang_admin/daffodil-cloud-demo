package com.daffodil.demo.service;

import com.daffodil.core.service.IBaseEntityService;
import com.daffodil.demo.entity.Student;

/**
 * -学生信息Service接口
 * @author yweijian
 * @date 2023-03-27
 * @version 1.0
 * @description
 */
public interface IStudentService extends IBaseEntityService<String, Student> {

}
